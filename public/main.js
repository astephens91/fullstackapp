
const url = "http://localhost:3000/api/users"

let form = document.forms.namedItem("userdata");

form.addEventListener('submit', function(ev) {
    ev.preventDefault()
    let output = document.querySelector("div");
    let oData = new FormData(form)
    let jsonObject =  {};

    for (const [key, value] of oData.entries()){
        jsonObject[key] = value;
    }
    let jsonString = JSON.stringify(jsonObject)
 
    console.log(jsonString)



    async function postData(url = '', data = {}){
        
        let dta = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'Accept': 'application/json' 
            },
            body: data
        });

        if (dta.status == 201){
            output.innerHTML = "New User Created!";
            console.log("New User Created!")
        }
         else if (dta.status === 400) {
             output.innerHTML = "There was an input error in creating your user.  Make sure your username and password are at least characters long.  Also, be sure you've inputted a valid email address!";
             console.log("There was an input error in creating your user.  Make sure your username and password are at least characters long.  Also, be sure you've inputted a valid email address!")
        }
        else if (dta.status === 409) {
            output.innerHTML = "That username is already taken!";
            console.log("That username is already taken!")
        }
        else {
            output.innerHTML = "There was an error with your request"
            console.log("There was an error with your request")
        }


    }
    postData(url, jsonString)

    // let req = new  XMLHttpRequest();
    // req.open("POST", url, true)
    // req.onload = function(submitEvent){
    //     if (req.status == 201){
    //         output.innerHTML = "New User Created!";
    //         console.log("New User Created!")
    //     }
    //      else if (req.status === 400) {
    //          output.innerHTML = "There was an input error in creating your user.  Make sure your username and password are at least characters long.  Also, be sure you've inputted a valid email address!";
    //          console.log("There was an input error in creating your user.  Make sure your username and password are at least characters long.  Also, be sure you've inputted a valid email address!")
    //     }
    //     else if (req.status === 409) {
    //         output.innerHTML = "That username is already taken!";
    //         console.log("That username is already taken!")
    //     }
    //     else {
    //         output.innerHTML = "There was an error with your request"
    //         console.log("There was an error with your request")
    //     }
    // }
    // req.send(jsonString)
    
    }, false)
