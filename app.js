const Joi = require("joi");
const express = require("express");
const axios = require("./.gitignore/node_modules/axios")
const app = express();
app.use(express.json());
app.use(express.static('public'))

const users = [
    { id: 1, username: 'Junker', password: 'password', email: 'buttstuff@yahoo.com', name: 'Butt Stuffington' },
    { id: 2, username: 'Jerk', password: 'password1', email: 'stevemartinsux@yahoo.com', name: 'Steve Martin' }
]

const usernames = []

for(let index = 0; index< users.length; index++){
    usernames.push(users[index].username)
}

app.get('/', (req, res) => {
    res.send("Hello World")
})

app.get('/api/users', (req, res) =>{
    res.send(users);
})

//Can use for query
// app.get('/api/users/:id', (req, res) => {
//     res.send(query)
// })

app.post('/api/users', (req, res) => {
   console.log(req.body)

   //Failed Joi attempt
   //==================
    // const schema = {
    //     username: Joi.string().alphanum().min(3).max(30).required(),
    //     password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    //     email: Joi.string().email({ minDomainAtoms: 2 }),
    //     name: Joi.string() 
    // };
    // const result = Joi.validate(req.body, schema);

    
    
    // if (result.error){
    //     //400 Bad Request
    //     res.status(408).send(result.error.details[0].message)
    //     return;
    // }

    if (usernames.includes(req.body.username) === true){
        //409 Bad Request
        res.status(409).send("Username already taken")
        return;
    }

    const user = {
        id: users.length + 1, 
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        name: req.body.name
    }
    users.push(user);
    res.status(201).send(user);
})

app.get('/api/users/:id', (req, res) => {
    const user = users.find(c => c.id === parseInt(req.params.id));
    if (!user) res.status(404).send('User was not found');
    res.send(user);
    
})

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on Port ${port}...`))